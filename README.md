# XTuner微调个人小助手认知

XTuner微调个人小助手认知

环境配置与数据准备     
步骤 0. 使用 conda 先构建一个 Python-3.10 的虚拟环境   
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/3a5cac2b-e0ca-438b-8d5a-a7f902562cc0/image.png 'image.png')     


步骤 1. 安装 XTuner     

![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/7a3da52f-11b0-4d32-94c7-f5089004bfbd/image.png 'image.png')    

验证安装    


![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/449105e0-86df-4f34-a8a8-c835780e0700/image.png 'image.png')    

修改提供的数据    

步骤 1. 创建修改脚本    
我们写一个脚本生成修改我们需要的微调训练数据，在当前目录下创建一个 change_script.py 文件，内容如下：
  ![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/f1afd299-3131-4a49-a82f-745deee7e6d5/image.png 'image.png')    
  
  步骤 2. 执行脚本   
  ![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/88735c27-71e8-4675-aa33-53328cf78f1e/image.png 'image.png')    

步骤 3. 查看数据    
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/ce8ab49f-7fcb-4e87-8c0b-37bd5e94e96a/image.png 'image.png')     

训练启动    

步骤 0. 复制模型   
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/fb7ca7c7-e089-4820-a8dd-6fe8a751c5e7/image.png 'image.png')    


步骤 1. 修改 Config    

![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/0a274c3f-dc5f-4c1a-a4c6-a50cab8d9c44/image.png 'image.png')    

![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/de3a89e7-4e3d-484f-93b5-814ba9fb7fc1/image.png 'image.png')   
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/78a58ce0-68db-4441-b065-dc6a1540e6ea/image.png 'image.png')   
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/0f4e16c7-bd9e-44e4-ac51-955fa7eb92ad/image.png 'image.png')    

步骤 3. 权重转换   

![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/014484c0-1bd7-4cb5-a0d5-58f94d253eb4/image.png 'image.png')    
步骤 4. 模型合并      

![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/c48caf89-5182-40d6-a20e-e19eeb08ded6/image.png 'image.png')    

模型 WebUI 对话     
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/4f6b559a-2801-4ff6-9da1-a80d36bd371b/image.png 'image.png')   
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/bdaae0a9-be2f-4733-8b41-ea8b9b19653f/image.png 'image.png')    



进阶任务:将自我认知的模型上传到 HuggingFace    
![image.png](https://raw.gitcode.com/weixin_74448215/test7/attachment/uploads/230132a8-af91-4e67-b728-8899f85d1b74/image.png 'image.png')



